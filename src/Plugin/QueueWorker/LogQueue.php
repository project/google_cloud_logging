<?php

namespace Drupal\google_cloud_logging\Plugin\QueueWorker;

use DomainException;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerBase;
use Exception;
use Google\Cloud\Core\Timestamp;
use Google\Cloud\Logging\Logger;
use Google\Cloud\Logging\LoggingClient;
use Google\Cloud\Logging\PsrLogger;
use Psr\Log\LogLevel;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sends queued logs to Google Cloud Logging.
 *
 * @QueueWorker(
 *   id = "google_cloud_logging",
 *   title = @Translation("Google Cloud Logging"),
 *   cron = { "time" = 60 }
 * )
 */
class LogQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {
  /**
   * Map from Drupal log levels to GC log levels.
   */
  static $levelMap = [
    LogLevel::EMERGENCY => Logger::EMERGENCY,
    LogLevel::ALERT => Logger::ALERT,
    LogLevel::CRITICAL => Logger::CRITICAL,
    LogLevel::ERROR => Logger::ERROR,
    LogLevel::WARNING => Logger::WARNING,
    LogLevel::NOTICE => Logger::NOTICE,
    LogLevel::INFO => Logger::INFO,
    LogLevel::DEBUG => Logger::DEBUG,
    RfcLogLevel::EMERGENCY => Logger::EMERGENCY,
    RfcLogLevel::ALERT => Logger::ALERT,
    RfcLogLevel::CRITICAL => Logger::CRITICAL,
    RfcLogLevel::ERROR => Logger::ERROR,
    RfcLogLevel::WARNING => Logger::WARNING,
    RfcLogLevel::NOTICE => Logger::NOTICE,
    RfcLogLevel::INFO => Logger::INFO,
    RfcLogLevel::DEBUG => Logger::DEBUG,
  ];

  /**
   * Logger that sends entries to Google Cloud.
   *
   * @var \Google\Cloud\Logging\PsrLogger
   */
  protected $logger;

  /**
   * Work queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   Configuration.
   * @param string $plugin_id
   *   Plugin ID.
   * @param mixed $plugin_definition
   *   Plugin definition
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   Queue factory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, QueueFactory $queueFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    // Use in-memory batch logger to avoid one API request per entry.
    $this->logger = self::logger();

    $this->queue = $queueFactory->get('google_cloud_logging');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('queue'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected static function logger() {
    try {
      return (new LoggingClient())->psrBatchLogger('drupal');
    } catch (DomainException $ex) {
      \Drupal::logger('google_cloud_logging')->error('Logger couldn\'t be initialised: %msg', [ '%msg' => $ex->getMessage() ]);
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($item) {
    /**
     * @todo
     * Maybe use the queue to manually claim up to 1000 entries and write them
     * all together. otherwise there is the risk that some entries may not be
     * flushed until the next cron run.
     */
    try {
      if (!$this->logger) {
        $this->logger = self::logger();
        if (!$this->logger) return;
      }

      [
        'level' => $level,
        'message' => $message,
        'context' => $context,
        'request' => $request,
        'caller' => $caller,
        'time' => $time,
      ] = $item;

      $gcLevel = self::$levelMap[is_string($level) ? strtolower($level) : $level];

      if (!empty($context['@backtrace_string'])) {
        // Log exceptions in GC's format.
        $gcLevel = Logger::ERROR;

        $gcMessage = "PHP Notice: {type}: {_message}\nStack trace:\n{trace}";

        $gcContext = [
          'type' => $context['@type'] ?? 'Exception',
          '_message' => $context['@message'],
          'trace' => $context['@backtrace_string'],

          'context' => [
            'reportLocation' => [
              'filePath' => $context['%file'],
              'lineNumber' => $context['%line'],
              'functionName' => $context['%function'],
            ],
          ],

          'serviceContext' => [
            // Provide this even if empty as it's required.
            'service' => $this->logger->getMetadataProvider()->serviceId(),
          ],
        ];

        if ($version = $this->logger->getMetadataProvider()->versionId()) $gcContext['serviceContext']['version'] = $version;

        // This has to be duplicated for error reporting unfortunately.
        if ($request) {
          $gcContext['context']['httpRequest'] = [
            'method' => $request['method'],
            'url' => $request['url'],
            'userAgent' => $request['userAgent'],
            'remoteIp' => $request['remoteIp'],
          ];
          if (!empty($request['referer'])) $gcContext['context']['httpRequest']['referrer'] = $request['referer'];
        }

      }
      else {
        // For normal log entries, keep caller-defined context data.
        $gcContext = array_filter($context, function ($key) { return preg_match('/^[%@:]/', $key); }, ARRAY_FILTER_USE_KEY);
        $keys = array_keys($gcContext);

        // Wrap all placeholders in the message with `{}`.
        $gcMessage = strtr(
          $message,
          array_combine(
            $keys,
            array_map(
              function ($key) { return '{' . $key . '}'; },
              $keys
            )
          )
        );

        if (!empty($context['link'])) $gcContext['link'] = $context['link'];

        if ($caller) $gcContext['stackdriverOptions']['sourceLocation'] = $caller;
      }

      $gcContext['channel'] = $context['channel'];
      $gcContext['context']['user'] = intval($context['uid']);

      $gcContext['stackdriverOptions']['timestamp'] = new Timestamp($time);
      if ($request) {
        $gcContext['stackdriverOptions']['httpRequest'] = [
          'requestMethod' => $request['method'],
          'requestUrl' => $request['url'],
          'userAgent' => $request['userAgent'],
          'remoteIp' => $request['remoteIp'],
          'protocol' => $request['protocol'],
        ];
        if (!empty($request['referer'])) $gcContext['stackdriverOptions']['httpRequest']['referer'] = $request['referer'];
      }


      $this->logger->log($gcLevel, $gcMessage, $gcContext);
    } catch (Exception $err) {
      // Smother errors otherwise there could be an infinite log loop.
      \Drupal::messenger()->addWarning('Google Cloud Logging error @code : <code>@message</code>', ['@code' => $err->getCode(), '@message' => $err->getMessage()]);
    }
  }

}
