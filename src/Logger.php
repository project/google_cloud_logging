<?php

namespace Drupal\google_cloud_logging;

use DateTimeImmutable as DateTime;
use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\Core\Queue\QueueFactory;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * A PSR-3 logger that logs Drupal log entries to Google Cloud Logging.
 */
class Logger implements LoggerInterface {

  use RfcLoggerTrait;

  /**
   * Work queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new object.
   *
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   Queue factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Request stack.
   */
  public function __construct(QueueFactory $queueFactory, RequestStack $requestStack) {
    $this->queue = $queueFactory->get('google_cloud_logging');
    $this->requestStack = $requestStack;
  }

  /**
   * Write a log entry to the background queue.
   *
   * @param string|int $level Severity of the log entry.
   * @param string $message Message to log.
   * @param array $context Context data.
   */
  public function log($level, $message, array $context = []) {
    if ($context['channel'] === 'google_cloud_logging') {
      return;
    }

    if ($request = ($this->requestStack->getCurrentRequest() ?? NULL)) {
      $request = [
        'method' => $request->getMethod(),
        'url' => $request->getUri(),
        'userAgent' => $request->headers->get('user-agent'),
        'remoteIp' => $request->getClientIP(),
        'protocol' => $request->getProtocolVersion(),
        'referer' => $request->headers->get('Referer', '') ?? NULL,
      ];
    }

    foreach (debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS) as $frame) {
      if ((empty($frame['class']) || $frame['class'] !== get_class($this) && strpos($frame['class'], 'Drupal\\Core\\Logger\\') !== 0) && !empty($frame['file'])) {
        $caller = [
          'file' => $frame['file'],
          'line' => intval($frame['line'] ?? 0),
          'function' => (isset($frame['class']) ? $frame['class'] . '->' : '') . $frame['function'] . '()',
        ];
        break;
      }
    }

    $this->queue->createItem([
      'level' => $level,
      'message' => (string)$message,
      'context' => (new NormalizerFormatter())->format($context),
      'request' => $request,
      'caller' => $caller ?? NULL,
      'time' => new DateTime(),
    ]);
  }

}
