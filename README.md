# Google Cloud Logging

A service that adds a logger to log everything to Google Cloud Logging, leaving the default DB logger in place

It uses [`google/cloud_logging`](https://github.com/googleapis/google-cloud-php-logging) to do all of the logging

## Init

Two environment variables need to be set for the logger to work:

- GOOGLE_APPLICATION_CREDENTIALS : Relative path from Drupal root to JSON file containing authentication credentials (e.g. : `../files/my-project-5115.json`)
- GOOGLE_CLOUD_PROJECT : Google Cloud Project name (e.g. : `my-gcp-project-5115`)

See the [Google Cloud authentication doc](https://github.com/googleapis/google-cloud-php/blob/master/AUTHENTICATION.md) for details
